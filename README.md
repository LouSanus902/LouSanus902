- 👋 Hi, you can call me Mike.
- 👀 I’m interested in all things IT.
-🌱 I’m currently learning the basics of administration, web design, networking and security.
- 💞️ I’m looking to collaborate on building my website and other projects.
- 📫 Contact me at jack@uofc.xyz

<!---
LouSanus902/LouSanus902 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
